var Matapath = function()
{
	// - modules
	//   + Bridge
	//   + BoardSimulator
	//   + GamePlayer
	//   + Controllers
	//   + UI
	//   - Util
	//     + js2cpp
	
	this._bridge = null;
	this._board_simulator = null;
	this._game_player = null;
	this._controllers = null;
	this._ui = null;
	this._util = { _mp: this }; // FIXME: extract class

	this._util.js2cpp = function(config)
	{
		var configs = {};

		var rune_name = ["water", "fire", "wood", "light", "dark", "heart"];

		configs.rune_priority = [];
		configs.rune_round_type = [];
		configs.rune_attack_type = [];
		configs.link_count = [];

		configs.rune_priority.push(0);
		configs.rune_round_type.push(0);
		configs.rune_attack_type.push(0);
		configs.link_count.push(0);

		for (var i = 0; i < rune_name.length; i++)
		{
			var rune_priority = config[rune_name[i] + "_priority"];
			configs.rune_priority.push(+rune_priority);

			var round_type = config[rune_name[i] + "_round_type"];
			configs.rune_round_type.push(+round_type);

			var attack_type = config[rune_name[i] + "_attack_type"];
			configs.rune_attack_type.push(+attack_type);
		}
		configs.column_attack_type = [1,1,1,1,1,1];
		for (var i = 0; i < 6; i++)
		{
			var content = "" + i;
			if (config.column_name.indexOf(content) != -1) {
				if (config.column_v5.indexOf(content) != -1) {
					configs.column_attack_type[i] = 5;
				} else if (config.column_v4.indexOf(content) != -1) {
					configs.column_attack_type[i] = 4;
				}
			} 	
		}
		configs.combo_count = config.board_final_combo == "自動" ? 0 : +config.board_final_combo;
		configs.first_combo_count = config.board_first_combo == "自動" ? 0 : +config.board_first_combo;
		configs.has_lowest_row = config.skill_bottom_clear == "On" ? true : false;
		configs.max_steps = +config.system_max_steps;
		configs.eight_direction_allowed = config.system_eight_direction == "On" ? true : false;
		configs.start_position = +config.skill_start_position;
		configs.ignited_path = +config.skill_etch_path;
		console.log(config.skill_random_wind);
		configs.max_solutions_size = 30 * 8 * 2 * 3 * +config.solution_size;
		if (config.skill_random_wind == "On" || config.skill_handle_question == "On") {
			configs.max_steps = 36;
			configs.max_solutions_size = 30 * 8 * 2 * 2;
		}
		configs.has_legency_greece = false;
		configs.keep_enhance = [0,0,0,0,0,0,0];
		for (var i in config.skill_legency_greece)
		{
			var value = config.skill_legency_greece[i];
			if (value == "水暗") {
				configs.has_legency_greece = true;
			} else if (value == "火") {
				configs.keep_enhance[2] = 1;
			} else if (value == "木") {
				configs.keep_enhance[3] = 1;
			}
		}
		configs.width = this._mp._ui._screen_width || 1080;
		configs.height = this._mp._ui._screen_height || 1920;
		return configs;
	};
};

Matapath.prototype.set_bridge = function(bridge)
{
	this._bridge = bridge;
}

Matapath.prototype.set_board_simulator = function(board_simulator)
{
	this._board_simulator = board_simulator;
}

Matapath.prototype.set_game_player = function(gameplayer)
{
	this._game_player = gameplayer;
}

Matapath.prototype.set_controllers = function(controllers)
{
	this._controllers = controllers;
}

Matapath.prototype.hide_view = function()
{
	this._bridge.callApp("hideDisplayView");
}

Matapath.prototype.show_view = function()
{
	this._bridge.callApp("showDisplayView");
}

Matapath.prototype.config = function()
{
	var configs = {};
	for (var ctrl in this._controllers)
	{
		configs[ctrl] = this._controllers[ctrl].value();
	}

	return configs;
}

Matapath.prototype.set_config = function(config)
{
	for (var ctrl in this._controllers)
	{
		this._controllers[ctrl].set_value(config[ctrl]);
	}
}
