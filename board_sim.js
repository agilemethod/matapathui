var BoardHtmlHelper = function()
{
}

BoardHtmlHelper.prototype.row_header = function()
{
	return '<div class="row"><div class="col-xs-12">';
}

BoardHtmlHelper.prototype.row_footer = function()
{
	return '</div></div>';
}

BoardHtmlHelper.prototype.pos2html = function(row, col)
{
	return 'bc_' + row + '_' + col;
}

BoardHtmlHelper.prototype.html2pos = function(html)
{
	html = html.split("_");
	if (html.length != 3) throw "syntax error in 'html2pos'";
	return { "row": +html[1], "col": +html[2] };
}

BoardHtmlHelper.prototype.col_header = function(row, col)
{
	return '<div id="' + this.pos2html(row, col) + '" class="col-xs-2 boardcell">';
}

BoardHtmlHelper.prototype.col_footer = function()
{
	return '</div>';
}

BoardHtmlHelper.prototype.dummy = function()
{
	return '<div class="dummy"></div>';
}

BoardHtmlHelper.prototype.rune_class = function(type)
{
	var WaterRune = 1;
	var FireRune = 2;
	var WoodRune = 3;
	var LightRune = 4;
	var DarkRune = 5;
	var HeartRune = 6;

	switch (type)
	{
		case WaterRune:
			return "water";
		case FireRune:
			return "fire";
		case WoodRune:
			return "wood";
		case LightRune:
			return "light";
		case DarkRune:
			return "dark";
		case HeartRune:
			return "heart";
		default:
			return "question";
	}
}

BoardHtmlHelper.prototype.board_cell = function(cell)
{
	/* -----------------------------------------------------
	 * <div class="dummy"></div>
	 * <div class="content puzzle">
	 * 	<div class="water">
	 * 		<span class="mark1 mark_rune_reserve"></span>
	 * 		<span class="mark1 mark_rune_priority"></span>
	 * 		<span class="mark2 mark_rune_unmove"></span>
	 * 		<span class="rune"></span>
	 * 	</div>
	 * </div>
	 * <div class="hit_area">
	 * </div>
	 * -----------------------------------------------------
	 */ 

	var html = this.dummy();
	html += '<div class="content';
	if (cell.puzzle)
	{
		html += ' puzzle';
	}
	html += '">';

	html += '<div class="' + this.rune_class(cell.rune.type);
	html += '">';
	if (cell.rune.priority == -1) // reserve
	{
		html += '<span class="mark1 mark_rune_reserve"></span>';
	}
	else if (cell.rune.priority == 1) // release
	{
		html += '<span class="mark1 mark_rune_priority"></span>';
	}

	if (!cell.rune.moveable)
	{
		html += '<span class="mark2 mark_rune_unmove"></span>';
	}

	html += '<span class="rune"></span>';
	html += '</div>';
	html += '</div>';

	html += '<div class="hit_area"></div>';
	return html;
}

BoardHtmlHelper.prototype.line = function(dr, dc)
{
	var line_name = "line_";
	var tb;
	var lr;

	if (dc > 0) lr = "r";
	else if (dc < 0) lr = "l";

	if (dr > 0) tb = "b";
	else if (dr < 0) tb = "t";

	if (dc == 0) lr = tb;
	if (dr == 0) tb = lr;
	line_name += (lr + tb);

	var html = '<div class="line ' + line_name + '">';
	html += '<span class="arrow"></span>';
	html += '</div>';
	return html;
}

var BoardSimulator = function(div_id)
{
	this._html_helper = new BoardHtmlHelper();
	this._jdom = $("#" + div_id);
	this._board = null;
	this._paths = [];
	this._ctrl_rune_selector = null;
	this._ctrl_rune_priority = null;
	this._ctrl_cell_status = null;
	this._background_is_transparent = true;
}

BoardSimulator.prototype.create = function(board)
{
	this._board = board;
	this.reset();
}

BoardSimulator.prototype.reset = function()
{
	this._paths = [];
	$("#board_status").html(this._paths.length);
	if (this._board) this.show(this._board);
	if (this._background_is_transparent)
	{
		this._jdom.css("background", "transparent");
	}
	else
	{
		this._jdom.css("background", "white");
	}
}

BoardSimulator.prototype.show = function(board)
{
	var div_html = "";
	for (var row = 0, row_count = board.cells.length; row < row_count; row++)
	{
		div_html += this._html_helper.row_header();
		for (var col = 0, col_count = board.cells[row].length; col < col_count; col++)
		{
			div_html += this._html_helper.col_header(row, col);
			div_html += this._html_helper.dummy();
			div_html += this._html_helper.board_cell(board.cells[row][col]);
			div_html += this._html_helper.col_footer();
		}
		div_html += this._html_helper.row_footer();
	}
	this._jdom.empty();
	this._jdom.append(div_html);
}

BoardSimulator.prototype.simulate = function(paths)
{
	this.reset();
	if (paths.length < 2) return;

	var from, to;
	for (var i = 2, l = paths.length; i <= l; i++)
	{
		from = paths[i-2];
		to = paths[i-1];
		this.swap_cell(from.row, from.col, to.row, to.col);
	}
}

BoardSimulator.prototype.swap_cell = function(from_row, from_col, to_row, to_col)
{
	if (this._paths.length <= 0)
	{
		// init
		this._paths.push({"row": from_row, "col": from_col});
	}

	var jfrom = this._jdom.find("#" + this._html_helper.pos2html(from_row, from_col));
	var jto = this._jdom.find("#" + this._html_helper.pos2html(to_row, to_col));
	var jsource = jfrom.find(".content");
	var jdestin = jto.find(".content");
	var tmp_html = jdestin.html();
	jdestin.html(jsource.html());
	jsource.html(tmp_html);

	if (this._paths.length > 1 &&
		this._paths[this._paths.length-2].row == to_row &&
		this._paths[this._paths.length-2].col == to_col)
	{
		// backward
		jto.children(".line").last().remove();
		this._paths.pop();

		if (this._paths.length == 1) this._paths.pop();
	}
	else
	{
		// forward
		this._paths.push({"row": to_row, "col": to_col});
		var line_html = this._html_helper.line(to_row - from_row, to_col - from_col);
		jfrom.append(line_html);
	}
	$("#board_status").html(this._paths.length);
}

BoardSimulator.prototype.update_cell_html = function(row, col)
{
	var j_boardcell = $("#"+this._html_helper.pos2html(row, col));
	j_boardcell.empty();
	j_boardcell.html(this._html_helper.board_cell(this._board.cells[row][col]));
}

BoardSimulator.prototype.update_cell = function(row, col)
{
	var boardcell = this._board.cells[pos.row][pos.col];
	var runetype = +this._ctrl_rune_selector.value();
	if (runetype > 0)
		boardcell.rune.type = +this._ctrl_rune_selector.value();
	boardcell.rune.priority = +this._ctrl_rune_priority.value();
	var unmove = this._ctrl_cell_status.value().indexOf("unmove");
	boardcell.rune.moveable = unmove >= 0 ? false : true;
	var puzzle = this._ctrl_cell_status.value().indexOf("puzzle");
	boardcell.puzzle = puzzle >=0 ? true : false;

	this.update_cell_html(row, col);
}

BoardSimulator.prototype.has_setting = function()
{
	if (this._ctrl_rune_selector.value() != "0") return true;
	if (this._ctrl_cell_status.value().length > 0) return true;
	return false;
}

BoardSimulator.prototype.switch_background_color = function()
{
	if (this._background_is_transparent)
	{
		this._background_is_transparent = false;
		this._jdom.css("background", "white");
	}
	else
	{
		this._background_is_transparent = true;
		this._jdom.css("background", "transparent");
	}
}

BoardSimulator.prototype.lock_controller = function()
{
	this._ctrl_rune_selector.disable();
	this._ctrl_rune_priority.disable();
	this._ctrl_cell_status.disable();
}

BoardSimulator.prototype.unlock_controller = function()
{
	this._ctrl_rune_selector.enable();
	this._ctrl_rune_priority.enable();
	this._ctrl_cell_status.enable();
}

BoardSimulator.prototype.listen = function()
{
	var _this = this;

	var rolling = false;
	var start_row = 0;
	var start_col = 0;

	this._jdom.on("mousedown touchstart",
		function(e)
		{
			e.preventDefault();
			var j_rune = $(e.target);
			var j_boardcell = j_rune.parents(".boardcell");
			if (j_boardcell && j_boardcell.attr("id"))
			{
				pos = _this._html_helper.html2pos(j_boardcell.attr("id"));
				start_row = pos.row;
				start_col = pos.col;
				if (_this.has_setting())
				{
					_this.update_cell(pos.row, pos.col);
				}
				else
				{
					if (_this._paths.length > 0)
					{
						var last_cell = _this._paths[_this._paths.length-1];
						if (last_cell.row != pos.row || last_cell.col != pos.col)
						{
							rolling = false;
							return;
						}
					}
					rolling = true;
				}
			}
		}
	);

	function handle_move(target_dom)
	{
		if (target_dom.className == "hit_area")
		{
			var j_rune = $(target_dom);
			var dom_boardcell = j_rune.parents(".boardcell")[0];
			pos = _this._html_helper.html2pos(dom_boardcell.id);

			var vaild_move = Math.abs(pos.row - start_row) <= 1
				&& Math.abs(pos.col - start_col) <= 1;

			if (!vaild_move)
			{
				rolling = false;
				return;
			}

			if (start_row != pos.row || start_col != pos.col)
			{
				if (_this._paths.length == 0)
				{
					_this.lock_controller();
				}

				_this.swap_cell(start_row, start_col, pos.row, pos.col);
				start_row = pos.row;
				start_col = pos.col;
			}
		}
	}

	function handle_cancel(target_dom)
	{
		rolling = false;
		if (_this._paths.length == 0)
		{
			_this.unlock_controller();

			var j_rune = $(target_dom);
			var j_boardcell = j_rune.parents(".boardcell");
			if (j_boardcell && j_boardcell.attr("id"))
			{
				pos = _this._html_helper.html2pos(j_boardcell.attr("id"));
				if (start_row == pos.row && start_col == pos.col)
				{
					_this.update_cell(start_row, start_col);
				}
			}
		}
	}

	this._jdom.on("mousemove",
		function(e)
		{
			e.preventDefault();
			var j_rune = $(e.target);
			var j_boardcell = j_rune.parents(".boardcell");
			if (!rolling) return;
			handle_move(e.target);
		}
	);

	this._jdom.on("touchmove",
		function(e)
		{
			e.preventDefault();
			if (!rolling) return;
			var loc = e.originalEvent.changedTouches[0];
			var real_target = document.elementFromPoint(loc.clientX, loc.clientY);
			handle_move(real_target);
		}
	);

	this._jdom.on("mouseup touchend",
		function(e)
		{
			e.preventDefault();
			handle_cancel(e.target);
		}
	);

	this._jdom.on("mouseleave touchcancel",
		function(e)
		{
			e.preventDefault();
			handle_cancel(e.target);
		}
	);
}
