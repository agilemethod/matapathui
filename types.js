function Rune(type, priority, moveable)
{
	this.type = type;
	this.priority = priority;
	this.moveable = moveable == undefined || false;
}

function BoardCell(rune, puzzle)
{
	this.rune = rune;
	this.puzzle = puzzle || false;
}

function Board()
{
	this.cells = [];
}

Board.prototype.load_array = function(array)
{
	var index = 0;
	for (var r = 0; r < 5; r++)
	{
		var row = [];
		for (var c = 0; c < 6; c++, index++)
		{
			row.push(new BoardCell(new Rune(array[index], 0)));
		}
		this.cells.push(row);
	}
}

Board.prototype.load_str = function(str)
{
	throw "not implemented";
}

Board.prototype.load = function(data)
{
	if (data instanceof Array)
	{
		this.load_array(data);
	}
	else if (typeof data == typeof "")
	{
		this.load_str(data);
	}
}

function TestCaseGenerator()
{
}

TestCaseGenerator.prototype.read_board_done = function()
{
	var WaterRune = 1;
	var FireRune = 2;
	var WoodRune = 3;
	var LightRune = 4;
	var DarkRune = 5;
	var HeartRune = 6;

	var board = new Board();
	board.cells =
	[
		[
		 	new BoardCell(new Rune(FireRune, 0, false)),
		  	new BoardCell(new Rune(HeartRune, 0, false)),
		    new BoardCell(new Rune(WoodRune, -1, false)),
		    new BoardCell(new Rune(WaterRune, 0)),
		    new BoardCell(new Rune(FireRune, 0)),
		    new BoardCell(new Rune(WaterRune, 0)),
		],
		[
		 	new BoardCell(new Rune(WoodRune, -1)),
		  	new BoardCell(new Rune(FireRune, 0)),
		    new BoardCell(new Rune(WaterRune, 0)),
		    new BoardCell(new Rune(LightRune, 0)),
		    new BoardCell(new Rune(LightRune, 0)),
		    new BoardCell(new Rune(HeartRune, 0)),
		],
		[
		 	new BoardCell(new Rune(WaterRune, 0)),
		  	new BoardCell(new Rune(HeartRune, 0)),
		    new BoardCell(new Rune(LightRune, 0), true),
		    new BoardCell(new Rune(HeartRune, 0)),
		    new BoardCell(new Rune(HeartRune, 0), true),
		    new BoardCell(new Rune(LightRune, 0)),
		],
		[
		 	new BoardCell(new Rune(DarkRune, 0)),
		  	new BoardCell(new Rune(FireRune, 0)),
		    new BoardCell(new Rune(WoodRune, -1), true),
		    new BoardCell(new Rune(DarkRune, 0), true),
		    new BoardCell(new Rune(WaterRune, 0), true),
		    new BoardCell(new Rune(HeartRune, 0)),
		],
		[
		 	new BoardCell(new Rune(FireRune, 1)),
		  	new BoardCell(new Rune(LightRune, 1)),
		    new BoardCell(new Rune(WaterRune, 1), true),
		    new BoardCell(new Rune(DarkRune, 1)),
		    new BoardCell(new Rune(HeartRune, 1), true),
		    new BoardCell(new Rune(LightRune, 1)),
		]
	];

	return board;
}
