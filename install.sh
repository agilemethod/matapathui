if [ ! -d thirdparty ]; then
	mkdir thirdparty
fi

cd thirdparty
git clone https://github.com/dcodeIO/bytebuffer.js.git
git clone https://github.com/dcodeIO/protobuf.js.git
