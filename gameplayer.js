var GamePlayer = function(matapath)
{
	this._auto_playing = false;
	this._mp = matapath;
};

GamePlayer.prototype.bridge = function()
{
	return this._mp._bridge;
}

GamePlayer.prototype.board_simulator = function()
{
	return this._mp._board_simulator;
}

GamePlayer.prototype.get_board_margin = function()
{

}

GamePlayer.prototype.load_board = function()
{
	var request = {};
	request["request"] = "load_board";
	request["config"] = this._mp._util.js2cpp(this._mp.config());
	console.log("cmd" + JSON.stringify(request));
	var ret = this.bridge().call(JSON.stringify(request));
	console.log("cmd done " + ret);
	ret = JSON.parse(ret);
	this.board_simulator().create(ret["board"]);
	console.log(JSON.stringify(this.board_simulator()._paths));
}

GamePlayer.prototype.calculate_path = function()
{
	var request = {};
	request["request"] = "calculate_path";
	request["board"] = this.board_simulator()._board;
	request["previous_path"] = this.board_simulator()._paths;
	request["config"] = this._mp._util.js2cpp(this._mp.config());

	console.log(JSON.stringify(this.board_simulator()._paths));
	console.log("cmd" + JSON.stringify(request));
	var ret = this.bridge().call(JSON.stringify(request));
	console.log("cmd done " + ret);
	ret = JSON.parse(ret);

	this.board_simulator().create(request["board"]);
	this.board_simulator().simulate(ret["paths"]);

	var status_html = " ";
	status_html += ret["matches"].length;
	status_html += "combo ";
	for (var i = 0; i < ret["matches"].length; i++)
	{
		status_html += '<span class="' + this.board_simulator()._html_helper.rune_class(ret["matches"][i].type) + '"></span>';
		status_html += ret["matches"][i].count;
	}
	console.log(status_html);

	$("#board_status").append(status_html);
}

GamePlayer.prototype.play = function()
{
	var request = {};
	request["request"] = "run_path";
	request["previous_path"] = this.board_simulator()._paths;
	request["config"] = this._mp._util.js2cpp(this._mp.config());

	console.log(JSON.stringify(this.board_simulator()._paths));
	console.log("cmd", request);
	var ret = Bridge.call(JSON.stringify(request));
	console.log("cmd done " + ret);
	ret = JSON.parse(ret);
}

GamePlayer.prototype.peek_and_play = function()
{
	var request = {};
	request["request"] = "peek_and_play";
	request["config"] = this._mp._util.js2cpp(this._mp.config());
	console.log("Matapath", "cmd", request);
	var ret = Bridge.call(JSON.stringify(request));
}

GamePlayer.prototype.unknown_rune_count = function()
{
	var unknown_number = 0;
	var cells = this.board_simulator()._board.cells;
	for (var i = 0; i < 5; i++) {
		for (var j = 0; j < 6; j++) {
			var type = cells[i][j].rune.type;
			if (type == 0 || type == 7) {
				unknown_number++;
			}
		}
	}
	return unknown_number;
}

GamePlayer.prototype.board_has_dialog = function()
{
	var unknown_rune_count = this.unknown_rune_count();
	if (unknown_rune_count > 6 && unknown_rune_count < 24)
	{
		var cells = this.board_simulator()._board.cells;
		for (var j = 0; j < 6; j++)
		{
			var type = cells[0][j].rune.type;
			if (type == 0 || type == 7)
			{
				return false;
			}
		}
		return true;
	}
	return false;
}

GamePlayer.prototype.escape_dialog = function()
{
	this.gesture_tap(500, 1250, 300);
}

GamePlayer.prototype.board_is_stable = function()
{
	console.log("Matapath " + "============board_is_stable");
	
	if (this.board_has_dialog())
	{
		this.escape_dialog();
		console.log("Dialog ignore...");
		return false;
	}

	if (this.unknown_rune_count() > 6)
	{
		return false;
	}

	var request = {};
	request["request"] = "board_is_stable";
	request["config"] = this._mp._util.js2cpp(this._mp.config());
	var ret = this.bridge().call(JSON.stringify(request));
	console.log(ret);
	ret = JSON.parse(ret);
	console.log("Matapath board_is_stable " + ret["result"]);

	return ret["result"];
}

GamePlayer.prototype.auto_play = function()
{
	if (!this._auto_playing) return;
	this.load_board();
	if (this.board_is_stable())
	{
		this.calculate_path();
		this.play();
	}

	setTimeout(this.auto_play.bind(this), +this._mp.config().system_detect_interval * 1000);
}

GamePlayer.prototype.start_auto_play = function()
{
	this._auto_playing = true;
	this.auto_play();
}

GamePlayer.prototype.stop_auto_play = function()
{
	this._auto_playing = false;
}

GamePlayer.prototype.gesture_tap = function(x, y, interval)
{
	var request = {};
	request["request"] = "gesture";
	request["type"] = "tap";
	request["x"] = +x;
	request["y"] = +y;
	request["interval"] = +interval; // in millseconds
	
	console.log("cmd" + JSON.stringify(request));
	var ret = this.bridge().call(JSON.stringify(request));
	console.log("cmd done " + ret);
}
