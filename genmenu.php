<?php
$replace_js = array(
	array("types.js", 'document.write(\'\x3Cscript src="types.js?q=\' + random_number + \'">\x3C/script>\');'),
	array("controllers.js", 'document.write(\'\x3Cscript src="controllers.js?q=\' + random_number + \'">\x3C/script>\');'),
	array("board_sim.js", 'document.write(\'\x3Cscript src="board_sim.js?q=\' + random_number + \'">\x3C/script>\');'),
	array("gameplayer.js", 'document.write(\'\x3Cscript src="gameplayer.js?q=\' + random_number + \'">\x3C/script>\');'),
	array("matapath.js", 'document.write(\'\x3Cscript src="matapath.js?q=\' + random_number + \'">\x3C/script>\');'),
);

$menu = file_get_contents("menu.html");

foreach ($replace_js as $js) {
	$content = shell_exec("yui-compressor $js[0]");
	$js_content = "\n" . $content . "\n";
	$menu = str_replace($js[1], $js_content, $menu);
}

file_put_contents("player.html", $menu);
	
?>