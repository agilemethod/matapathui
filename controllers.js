var Controller = function(jdom)
{
	if (this.constructor == Controller)
		throw "abstract class: Controller";

	this._jdom = jdom;
	this._enabled = true;
}

Controller.prototype.install = function()
{
	throw "virtual method: Controller.install";
}

Controller.prototype.set_value = function(value)
{
	throw "virtual method: Controller.value";
}

Controller.prototype.value = function()
{
	throw "virtual method: Controller.value";
}

Controller.prototype.disable = function()
{
	this._enabled = false;
	this._jdom.children().each(function() {
		$(this).attr("disabled", true);
	});
}

Controller.prototype.enable = function()
{
	this._enabled = true;
	this._jdom.children().each(function() {
		$(this).attr("disabled", false);
	});
}

Controller.prototype.active_class = function()
{
	return "btn-primary";
}

Controller.prototype.inactive_class = function()
{
	return "btn-default";
}

var Toggle = function(jdom)
{
	Controller.apply(this, arguments);
}

Toggle.prototype = Object.create(Controller.prototype);
Toggle.prototype.constructor = Toggle;

var MultiSelect = function(jdom)
{
	Controller.apply(this, arguments);
}

MultiSelect.prototype = Object.create(Controller.prototype);
MultiSelect.prototype.constructor = MultiSelect;

MultiSelect.prototype.install = function()
{
	var _this = this;
	var active_class = this.active_class();
	var inactive_class = this.inactive_class();
	this._jdom.children(".btn").on("click", function() {
		if (!_this._enabled) return;
		var j_this = $(this);
		var j_parent = j_this.parent();
		if (j_this.hasClass(active_class)) {
			j_this.removeClass(active_class).addClass(inactive_class);
		} else {
			j_this.removeClass(inactive_class).addClass(active_class);
		}
	});
}

MultiSelect.prototype.set_value = function(values)
{
	for(var i in values) {
		this._jdom.find(".btn").each(function(){
			var val = $(this).attr("mp-data") || $(this).html();
			$(this).removeClass("btn-primary");
			$(this).removeClass("btn-default");
			if (val == values[i]) {
				$(this).addClass("btn-primary");
			} else {
				$(this).addClass("btn-default");
			}
		});
	}
}

MultiSelect.prototype.value = function()
{
	var data = [];
	var active_btn = this._jdom.find(".btn-primary").each(function(){
		var val = $(this).attr("mp-data");
		if (val) data.push(val);
		else data.push($(this).html());
	});
	return data;
}

var MultiRadio = function(jdom)
{
	Controller.apply(this, arguments);
}

MultiRadio.prototype = Object.create(Controller.prototype);
MultiRadio.prototype.constructor = MultiRadio;

MultiRadio.prototype.install = function()
{
	var _this = this;
	var active_class = this.active_class();
	var inactive_class = this.inactive_class();
	this._jdom.children(".btn").on("click", function() {
		if (!_this._enabled) return;
		var name = $(this).attr("name");
		$(".btn[name=" + name + "]").each(function() {
			$(this).removeClass(active_class).addClass(inactive_class);
		});
		$(this).removeClass(inactive_class).addClass(active_class);
	});
}

MultiRadio.prototype.set_value = function(values)
{
	for(var i in values) {
		this._jdom.find(".btn").each(function(){
			var val = $(this).attr("mp-data") || $(this).attr("name");
			$(this).removeClass("btn-primary");
			$(this).removeClass("btn-default");
			if (val == values[i]) {
				$(this).addClass("btn-primary");
			} else {
				$(this).addClass("btn-default");
			}
		});
	}
}

MultiRadio.prototype.value = function()
{
	var data = [];
	var active_btn = this._jdom.find(".btn-primary").each(function(){
		var val = $(this).attr("mp-data");
		if (val) data.push(val);
		else data.push($(this).attr("name"));
	});
	return data;
}

var Switch = function(jdom)
{
	Controller.apply(this, arguments);
}

Switch.prototype = Object.create(Controller.prototype);
Switch.prototype.constructor = Switch;

Switch.prototype.install = function()
{
	var _this = this;
	var active_class = this.active_class();
	var inactive_class = this.inactive_class();
	this._jdom.children(".btn").on("click", function() {
		if (!_this._enabled) return;
		_this._jdom.children(".btn").each(function() {
			$(this).removeClass(active_class).addClass(inactive_class);
		});
		$(this).removeClass(inactive_class).addClass(active_class);
	});
}

Switch.prototype.set_value = function(value)
{
	this._jdom.find(".btn").each(function(){
		var val = $(this).attr("mp-data") || $(this).html();
		$(this).removeClass("btn-primary");
		$(this).removeClass("btn-default");
		if (val == value) {
			$(this).addClass("btn-primary");
		} else {
			$(this).addClass("btn-default");
		}
	});
}

Switch.prototype.value = function()
{
	var active_btn = this._jdom.find(".btn-primary");
	var val = active_btn.attr("mp-data");
	return val ? val : active_btn.html();
}

var Stepper = function(jdom)
{
	Controller.apply(this, arguments);
}

Stepper.prototype = Object.create(Controller.prototype);
Stepper.prototype.constructor = Stepper;

Stepper.prototype.install = function()
{
	var _this = this;
	var stepper_minus = "stepper-minus";
	var stepper_add = "stepper-add";
	this._jdom.find(".btn").on("click", function() {
		if (!_this._enabled) return;
		var j_this = $(this);
		var j_parent = j_this.parents(".stepper");
		var data_state_id = +j_parent.attr("stepper-state-id");
		var data_model = j_parent.attr("stepper-data-model");
		var contents = JSON.parse(data_model);
		if (j_this.hasClass(stepper_add)) {
			data_state_id++;
		} else {
			data_state_id--;
			if (data_state_id < 0) data_state_id = data_state_id + contents.length;
		}
		var value = contents[(data_state_id) % contents.length];
		j_parent.find(".stepper-content").html(value);
		j_parent.attr("stepper-state-id", data_state_id);
	});
}

Stepper.prototype.set_value = function(value)
{
	var data_model = JSON.parse(this._jdom.attr("stepper-data-model"));
	for (var i in data_model) {
		if (data_model[i] == value) {
			this._jdom.attr("stepper-state-id", i);
			this._jdom.find(".stepper-content").html(value);
			break;
		}
	}
}

Stepper.prototype.value = function()
{
	var j_content = this._jdom.find(".stepper-content");
	return j_content.html();
}

function ControllerFactor(jdom)
{
	var controller;

	var none_action = "non-action"
	var toggle_class = "toggle-btn";
	var multi_class = "multi-btn";
	var radio_by_name_class = "radio-by-name";
	var stepper_class = "stepper";
	if (jdom.hasClass(none_action))
	{
		controller = null;
	}
	else if (jdom.hasClass(toggle_class))
	{
		controller = new Toggle(jdom);
	}
	else if (jdom.hasClass(multi_class))
	{
		controller = new MultiSelect(jdom);
	}
	else if (jdom.hasClass(radio_by_name_class))
	{
		controller = new MultiRadio(jdom);
	}
	else if (jdom.hasClass(stepper_class))
	{
		controller = new Stepper(jdom);
	}
	else
	{
		controller = new Switch(jdom);
	}

	return controller;
}
